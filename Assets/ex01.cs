﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class ex01 : MonoBehaviour {

	// Use this for initialization
	void Start () {

		FileStream F = new FileStream (Application.persistentDataPath + "/ejercicio01.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);

		byte[] a=new byte[4];

		for (int i =1; i<=2000; i++) {
	
			a= BitConverter.GetBytes(i);
			F.Write(a,0,4);
		}

		F.Position = 0;

		for (int j=0; j<2000; j++) {

			F.Read (a,0,4);
			int tmp=BitConverter.ToInt32(a,0);

			Debug.Log (tmp);
		}
		F.Flush ();
		F.Close ();

	}
	// Update is called once per frame
	void Update () {
	
	}
}
